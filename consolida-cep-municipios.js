const connection = require('../managers/database-manager');
const fs = require('fs');
const parse = require('csv-parse');

const Municipio = require('../models/logradouros/municipio');
const {
  Localidade,
  Distrito,
  SubDistrito,
  Bairro
} = require('../models/logradouros');

module.exports = function (manager, next) {
   if(next) next(manager);
}