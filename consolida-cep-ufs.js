const connection = require('../managers/database-manager');
const fs = require('fs');
const parse = require('csv-parse');


const {
  UF,
  Municipio,
  Localidade,
  Distrito,
  SubDistrito,
  Bairro
} = require('../models/logradouros');

const {
  googleResultGeoCode
} = require('../models/google')

/**
 * 
 *  "status": {
      type: String,
      enum: ["OK", // indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.
         "ZERO_RESULTS", //indicates that the geocode was successful but returned no results. This may occur if the geocoder was passed a non-existent address.
         "OVER_DAILY_LIMIT", //indicates any of the following:
         //                         The API key is missing or invalid.
         //                         Billing has not been enabled on your account.
         //                         A self - imposed usage cap has been exceeded.
         //                         The provided method of payment is no longer valid(for example, a credit card has expired).
         "OVER_QUERY_LIMIT", // indicates that you are over your quota.
         "REQUEST_DENIED", // indicates that your request was denied.
         "INVALID_REQUEST", // generally indicates that the query (address, components or latlng) is missing.
         "UNKNOWN_ERROR" //indicates that the request could not be processed due to a server error. The request may succeed if you try again.
      ]
   }
 */
// https://maps.googleapis.com/maps/api/geocode/json?address=%27cep%2061700000%27&key=AIzaSyBw73RAUGYm4L0LSmqA7kS5YFsyabq5ZCk&language=jp
var googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyBw73RAUGYm4L0LSmqA7kS5YFsyabq5ZCk',
  Promise: Promise,
  language: 'pt'
});
//UF.findByCEP('61700000')
//.then(result=>{
googleMapsClient.geocode({
    address: 'CEP 61.7000-000'
  }).asPromise()
  .then(response => {
    console.log(JSON.stringify(response));
  }).catch(err => {
    console.log(`Erro ao consultar o google: ${err}`);
  });

//})

module.exports = function (manager, next) {
  if (next) next(manager);
}