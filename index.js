require('dotenv').config();

const connection = require('../managers/database-manager');
var fs = require('fs');

/*
const memwatch = require('memwatch-next');

memwatch.on('leak', (info) => {
   console.error('Memory leak detected:\n', info);
});

memwatch.on('stats', function(info) { 
   console.error('Stats:\n', info);
});
*/

const {
   UF,
   Municipio,
   Localidade,
   SubDistrito,
   Distrito,
   Logradouro,
   Bairro
} = require('../models/logradouros');

this.localidades = [];
this.municipios = [];
this.ufs = [];

connection.once('open', function () {
   console.log("-------------------------")
   console.log("Contabilizando Registros!");
   UF.countDocuments((err, count) => {
      console.log("%d UFs Cadastrados", count);
   });
   Municipio.countDocuments((err, count) => {
      console.log("%d Municipios Cadastrados", count);
   });
   Distrito.countDocuments((err, count) => {
      console.log("%d Distritos Cadastrados", count);
   });
   SubDistrito.countDocuments((err, count) => {
      console.log("%d Subdistritos Cadastrados!", count)
   });
   Bairro.countDocuments((err, count) => {
      console.log("%d Bairros Cadastrados", count);
   });
   Logradouro.countDocuments((err, count) => {
      console.log("%d Logradouros Cadastrados", count);
   });
   console.log("-------------------------");
});

console.log("-------------------------");
console.log("Iniciando povoamento da base relativa aos logradouros!");
require('./popula-uf')(this, () => {
   console.log("Iniciando processamento de Municipios!");
   require('./popula-municipios')(this, () => {
      console.log("Iniciando processamento de Distritos!");
      require('./popula-distrito')(this, () => {
         console.log("Iniciando processamento de Subdistritos!");
         require('./popula-sub-distrito')(this, () => {
            console.log("Iniciando processamento de Bairros!");
            require('./popula-bairros')(this, () => {
               console.log("Iniciando consolidação dos CEPs Estaduais!");
               require('./consolida-cep-ufs')(this, () => {
                  console.log("Iniciando consolidação dos CEPs Municipais!");
                  require('./consolida-cep-municipios')(this, () => {
                     console.log("Iniciando processmaento de Logradouros por CEP!");
                     require('./popula-logradouro')(this, () => {
                        console.log("Cadeia de Processamento finalizado, podem haver promises pendentes!")
                     });
                  })
               });
            });
         });
      });
   });
});
console.log("-------------------------")