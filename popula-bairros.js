const connection = require('../managers/database-manager');
const fs = require('fs');
const parse = require('csv-parse');

const Municipio = require('../models/logradouros/municipio');
const {
  Localidade,
  Distrito,
  SubDistrito,
  Bairro
} = require('../models/logradouros');

module.exports = function (manager, next) {
  let csvData = [];
  console.log("Iniciando Processamento de Bairros!");
  let dataFile = './dados/localities.csv';
  if (fs.existsSync(dataFile))
    fs.createReadStream(dataFile)
    .pipe(
      parse({
        delimiter: ',',
        // headers: [ibge_city_code,ibge_code,name ]
      })
    )
    .on('headers', header => {
      console.log('Cabeçalho Bairros: ' + header);
    })
    .on('data', csvrow => {
      if (csvrow[1] === 'ibge_code') {
        console.log('Cabeçalho Bairros: ' + csvrow);
        return;
      }
      csvData.push(csvrow);
    })
    .on('end', async function () {
      console.log('Leitura do Arquivo de Localizades finalizada, linhas processadas: ' + csvData.length);

      let ibgeCityCode = [];

      let listBairroNaoEncontradas = [];
      let listaBuscaBairro = [];

      let listaBuscaMunicpioBairros = [];
      let bairroUpdate =[];
      let bairroSave = []
      //  console.log(csvData);

      let totalCount = count = 0;
      for (let row of csvData) {
        //       0           1       2
        //ibge_city_code,ibge_code,name
        //1100015,110001505001,Centro
        const ibgeCode = row[1];
        ibgeCityCode[ibgeCode] = row[0];
        const nome = row[2];
        // console.log("Dados: " + csvrow);
        listaBuscaBairro.push(
          await Bairro.findByIdIBGE(ibgeCode)
          .then(bairro => {
            if (++count > 1000) {
              totalCount += count;
              count = 0;
              console.log("Processados 1000 Localiddes: " + totalCount);
            }
            //  console.log(bairro);
            if (!bairro) {
              listBairroNaoEncontradas.push({
                idIBGE: ibgeCode,
                nome: nome,
              });
            }
          })
        );
      }

      console.log("Processando a lista de busca!");
      // console.log(listNaoBairrosEncontradas);
      let race = Promise.all(listaBuscaBairro);
      console.log("Processamento da lista de busca, pronto!");

      totalCount = count = 0;

      if (listBairroNaoEncontradas.length > 0) race.then(
          await Bairro.create(listBairroNaoEncontradas)
          .then(allLocal => {
            for (let bairro of allLocal) {
              (function (bairro) {
                if (++count > 1000) {
                  totalCount += count;
                  count = 0;
                  console.log("Processados 1000 Localiddes: " + totalCount);
                }
                const idIBGE = bairro.idIBGE;
                const idIBGEMunicipio = ibgeCityCode[idIBGE];
                //  console.log(id2, ' - ', id)
                listaBuscaMunicpioBairros.push(Municipio.findByIdIBGE(idIBGEMunicipio)
                  .then(m => {
                    if (m) {
                      bairro.municipio = m;
                      bairroUpdate.push(bairro);
                    }
                  })
                  .catch(err => {
                    console.log(err);
                    throw err;
                  }));
              })(bairro);
            }
            return allLocal;
          })
          .then(allLocal => {
            console.log('Bairros salvos: ' + (allLocal ? allLocal.length : 0));
            if (next) next(manager);
          })
          .catch(err => {
            throw err;
          })
        )
        .then(await Promise.all(listaBuscaMunicpioBairros))
        .then(() => {
          for (let bairro of bairroUpdate) {
            bairroSave.push(bairro.save()
              .catch(err => {
                console.log(err);
                throw err;
              }))
          }
        })
        .then(Promise.all(bairroSave))
        .then(() => {
          console.log('Bairros atualizados:' + bairroUpdate ? bairroUpdate.length : 0);
        }).catch(err => {
          console.log(err);
          throw err;
        })
      race.then(() => {
          if (next) next(manager);
        })
        .catch(err => {
          console.log(err);
          throw err;
        })
    });
  else if (next) next(manager);
};