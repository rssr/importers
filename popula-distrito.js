var fs = require('fs');
var parse = require('csv-parse');

const Municipio = require('../models/logradouros/municipio');
const {
  Distrito
} = require('../models/logradouros/localidade');

module.exports = function (manager, next) {
  let csvData = [];
  console.log("Iniciando Processamento de Distritos!");
  let dataFile = './dados/districts.csv';
  if (fs.existsSync(dataFile))
    fs.createReadStream(dataFile)
    .pipe(
      parse({
        delimiter: ',',
        // headers: [ ibge_code,ibge_city_code,name ]
      })
    )
    .on('headers', header => {
      console.log('Cabeçalho districts: ' + header);
    })
    .on('data', csvrow => {
      if (csvrow[0] === 'ibge_code') {
        console.log('Cabeçalho districts: ' + csvrow);
        return;
      }
      csvData.push(csvrow);
    })
    .on('end', async function () {
      console.log('Leitura do Arquivo districts finalizada, linhas processadas: ' + csvData.length);
      let ibgeCityCode = [];
      let listDistritosNaoEncontrados = [];
      let listaBuscaDistrito = [];
      for (let row of csvData) {
        //      0             1       2
        //ibge_code,ibge_city_code,name
        //110001505,1100015,Alta Floresta d'Oeste
        const ibgeCode = row[0];
        ibgeCityCode[ibgeCode] = row[1];
        const nome = row[2];

        // console.log("Dados: " + csvrow);
        listaBuscaDistrito.push(
          await Distrito.findByIdIBGE(ibgeCode)
          .then(distrito => {
            if (!distrito) {
              listDistritosNaoEncontrados.push({
                idIBGE: ibgeCode,
                nome: nome,
              });
            }
          })
        );
      }
      let dsmUpdate = [];
      let dsmSave = [];
      let listFindMunicipioDistrito = [];
      let race = Promise.all(listaBuscaDistrito);
      if (listDistritosNaoEncontrados.length > 0) race.then(
          await Distrito.create(listDistritosNaoEncontrados)
          .then(allDsM => {
            for (let dsm of allDsM) {
              (function (dsm) {
                const idIBGE = dsm.idIBGE;
                const idIBGEMunicipio = ibgeCityCode[idIBGE];
                //  console.log(id2, ' - ', id)
                listFindMunicipioDistrito.push(Municipio.findByIdIBGE(idIBGEMunicipio)
                  .then(m => {
                    if (m) {
                      dsm.municipio = m;
                      dsmUpdate.push(dsm);
                    }
                  })
                  .catch(err => {
                    console.log(err);
                    throw err;
                  }));
              })(dsm);
            }
            return allDsM;
          })
          .then((allDsM) => {
            console.log('Distritos salvos:' + allDsM ? allDsM.length : 0);

          })
          .catch(err => {
            console.log(err);
            throw err;
          })
        )
        .then(await Promise.all(listFindMunicipioDistrito))
        .then(() => {
          for (dsm of dsmUpdate) {
            dsmSave.push(dsm.save()
              .catch(err => {
                console.log(err)
                throw err;
              }));
          }
          return dsmUpdate;
        })
        .then(Promise.all(dsmSave))
        .then(() => {
          console.log('Distritos atualizados:' + dsmUpdate ? dsmUpdate.length : 0);
        }).catch(err => {
          console.log(err);
          throw err;
        })
      race.then(() => {
          if (next) next(manager);
        })
        .catch(err => {
          console.log(err);
          throw err;
        })
    });
  else if (next) next(manager);
};