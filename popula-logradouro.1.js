const fs = require('fs');
const path = require('path');
const parse = require('csv-parse');
const connection = require('../managers/database-manager');

const Municipio = require('../models/logradouros/municipio');
const {
  TipoLocalidade,
  Localidade,
  Distrito,
  SubDistrito,
  Bairro,
  UF,
  TipoLogradouro,
  Logradouro,
  TIPO_CEP
} = require('../models/logradouros');

const MONGO_QUERY_TIMEOUT = 2000;
/**
 * Quando não se encontra o tipo definido, pode-se verificar no nome, se ele começa com alguma referência ao tipo.
 * 
 * Para isso, basta obter o primeiro token do nome, e compara-lo a uma das abreviaturas ou nome do tipo de logradouro.
 * 
 * @param {String} nome Nome Logradouro 
 */
async function extractTipoLogradouroFromNome(nome) {
  let tipo;
  if (nome) {
    const tokens = nome.trim().split(' ');
    if (tokens && tokens.length >= 1) {
      for (let token of tokens) {
        token = token.trim();
        token = token.replace(/[:,;]$/, '');
        // console.log("Pesquisando Tipo de Logradouro: %s para o logradouro: %s", token, nome);
        if (token.endsWith('.')) tipo = await extractTipoLogradouroFromTokenAbreviatura(token);
        else tipo = await extractTipoLogradouroFromToken(token);

        if (tipo) break;
      }
    }
  }
  if (!tipo) tipo = await TipoLogradouro.desconhecido();
  // console.log("Tipo extraido: " + tipo);
  return tipo;
}

async function extractTipoLogradouroFromToken(nome) {
  let tipo;
  if (nome) {
    nome = nome.trim();
    tipo = await TipoLogradouro.findOneByNome(nome);
  }
  return tipo;
}

async function extractTipoLogradouroFromTokenAbreviatura(abreviatura) {
  let tipo;
  if (abreviatura) {
    abreviatura = abreviatura.trim();
    tipo = await TipoLogradouro.findOneByAbreviatura(abreviatura);
  }
  return tipo;
}

/**
 * Com base na localização informada e configuração do sistema retorna uma Localidade (Localidade, Distrito, SubDistrito) para ser usado.
 * 
 * Retorna o Objeto Localidade ou um de Seus Discriminators.
 * 
 * @param {Objet} localizacao  * {cep, 
 *  uf:          {uf,cep_inicial, cep_final}, 
 *  municpio:    {municipio, cep_inicial, cep_final}, 
 *  bairro:      {bairro, cep_inicial, cep_final}, 
 *  subdistrito: {subdistrito,cep_inicial, cep_final},
 *  distrito:    {distrito,,cep_inicial, cep_final}
 * } 
 */
function selecionaLocalidade(localizacao) {
  return null;
}
/**
 * Com base no CEP Informado descobre UF, Municipio, Localidade, SubDistrito e Distrito.
 * 
 * Caso uma das localizações não seja informado apenas não a usa para ajudar a descobrir as informações.
 * 
 * Se CEP não for infomrado é preciso que pelo menos uma das Localizações sejam informadas para retornar a faixa de CEP correspondnete.
 * 
 * Retorna um objeto com 
 * {cep, 
 *  uf:          {uf,cep_inicial, cep_final}, 
 *  municpio:    {municipio, cep_inicial, cep_final}, 
 *  bairro:      {bairro, cep_inicial, cep_final}, 
 *  subdistrito: {subdistrito,cep_inicial, cep_final},
 *  distrito:    {distrito,,cep_inicial, cep_final}
 * }, conforme forem identificado.
 * @param {*} cep 
 * @param {*} localização {uf, municipio, localidade, subdistrito, distrito } alguns ou todos
 */
function processaCEP(cep, {
  uf,
  municipio,
  bairro,
  subdistrito,
  distrito
}) {
  return null;
}

let totalcountLogr, countLogr, totalcountLogrSalvos, countLogrSalvos;
module.exports = function (manager, next) {
  let dataDir = path.join(__dirname, '../dados/ceps');
  let processedDataDir = path.join(__dirname, '../dados/INSERIDOS/ceps.processados');
  console.log("Iniciando Processamento de Logradouros e CEPs, na pasta: " + dataDir);
  fs.readdir(dataDir, function (err, fileNames) {
    if (err) {
      console.log(err);
      return;
    }
    fileNames.forEach(function (fileName) {

      const cepsDir = path.join(dataDir, fileName);
      const ufDir = fileName;
      if (fs.statSync(cepsDir).isDirectory() && ufDir.length == 2) {
        console.log("Processando a pasta: " + cepsDir);
        fs.readdir(path.join(dataDir, fileName), function (err, dataFiles) {
          dataFiles.forEach(function (dataFile) {
            console.log("Verificando o aquivo: " + dataFile);
            if (path.extname(dataFile) == '.csv') {
              let lines;
              try {
                lines = processFile(path.join(dataDir, fileName, dataFile), path.join(processedDataDir, fileName, dataFile));
              } catch (err) {
                console.log("Falha ao processar o arquivo " + dataFile);
                console.log(err);
                return;
              }
            }
          })
        })
      }
    });
  });
}
let cacheBairro = [];
let cacheMunicipio = [];
let cacheTipoLogradouro = [];
let tipo = TipoLogradouro.desconhecido();
cacheTipoLogradouro[tipo.nome] = tipo;
let cacheUFs = [];

function processFile(dataFile,processedDataFile) {
  console.log("Processando Arquivos de CEP e Logradouros: " + dataFile)

  let countLineFile, totalCountLine, countLine;

  let fsReadStream = fs.createReadStream(dataFile, {
    autoClose: true
  })
  fsReadStream
    .pipe(
      parse({
        delimiter: ',',
      })
    ).on('open', open => {
      console.log(open);
      console.log(fsReadStream.bytesRead)
    }).on('ready', ready => {
      console.log(ready);
      console.log(fsReadStream.bytesRead)
    })
    .on('error', err => {
      console.log('Error ao tratar o arquivo %s: %s', dataFile, err);
    })
    .on('headers', header => {
      console.log('Cabeçalho Logradouros e CEPs: ' + header);
    })
    .on('data', async row => {
      if (row[0] === 'id') {
        console.log('Cabeçalho Logradouros e CEPs: ' + row);
        return;
      }

      //  0                  1                 2           3                4          5
      //"id"          ,"state_id:id"       ,"code"    ,"street"     ,"street_type","district"       ,"l10n_br_city_id:id"
      //"cep_69909110","l10n_br_base.br_ac","69909110","Santa Maria","Rua"        ,"Baixa da Colina","l10n_br_base.city_67"


      let cep = row[2];
      let cepUf;
      if (cep) cepUf = cep.slice(0, 1);
      let cepBairro;
      if (cep) cepBairro = cep.slice(0, 5);
      let nome = row[3];
      let nometipoLogradouro = row[4];
      let nomeLocalidade = row[5];
      let codigoMunicipioTemporario = row[6];
      if (codigoMunicipioTemporario)
        codigoMunicipioTemporario = codigoMunicipioTemporario.split('.')[1].split('_')[1]
      if (countLine > 1000) {
        console.log("Processado %d: ", totalCountLine);
        totalCountLine += countLine;
        countLine = 0;
      }
      countLine++;
      countLineFile++;

      if (!nome || nome.trim().length == 0) return;



      let tipo = await encontraTipoLogradouro(nometipoLogradouro, nome, row);
      let uf = await encontraUF(row);
      let m = await encontraMunicipio(cep, row);
      let b = await encontrarLocalidade(nomeLocalidade, row);
      let insert = await Logradouro.findByCep(cep)
        .then(logr => {
          // logr será um array, pois pode haver mais de um logradouro com o mesmo CEP
          if (Array.isArray(logr) && logr.length > 0) {
            console.log("Já há um Logradouro com este CEP, na linha %d, arquivo %s", countLine, dataFile);
            console.log("Verificar algortimo de consolidação dos CEPs no processo de mesmo nome!");
            // verifica se o logr tem CEP exclusivo e se pode usar por faixa
            for (let l of logr) {
              console.log("Logradouro: " + l);
              if (l.tipoCep == TIPO_CEP.EXCLUSIVO) {
                console.log("Não permite cadastro de multiplos Logradouros para este CEP!");
                return false;
              }
            }
          } else if (logr.tipoCep == TIPO_CEP.EXCLUSIVO) {
            console.log("Não permite cadastro de multiplos Logradouros para este CEP!");
            return false;
          }
          return true;
        })
        .catch(err => {
          console.log(`Erro ao verificar se Logradouro já foi cadastrado e suas condições de reuso do CEP, linha: `);
          console.log(row);
          console.log(err);
          throw err;
        });

      if (insert) {
        let logradouro = new Logradouro({
          nome,
          cep
        });
        if (countLogr > 1000) {
          console.log(`%d Logradouros processado.`, totalcountLogr);
          totalcountLogr += countLogr;
          countLogr = 0;
        }
        countLogr++;

        //                                  let localizacao = processaCEP(cep, uf, m, b, s, d)
        //                                  let localidade = selecionaLocalidade(localizacao);
        //                                  logradouro.localidade = localidade;
        logradouro.localidade = b;
        logradouro.tipo = tipo;
        logradouro.municipio = m;
        logradouro.uf = uf;
        logradouro.tipoCEP = TIPO_CEP.FAIXA
        logradouro.codMunicipioTemporario = codigoMunicipioTemporario;
        await logradouro.save()
          .then(() => {
            if (countLogrSalvos > 1000) {
              console.log(`%d Logradouros processado.`, totalcountLogrSalvos);
              totalcountLogrSalvos += countLogrSalvos;
              countLogrSalvos = 0;
            }
            countLogrSalvos++;
          })
          .catch((err) => {
            console.log("Erro ao salvar a linha: ");
            console.log(row);
            console.log(err);
            throw err;
          });
      }
    })
    .on('end', async function () {
      console.log(`Aquivo ${dataFile} Finalizado!`);
      fs.rename(dataFile, processedDataFile, function (err) {
        console.log(err);
        console.log(`${dataFile} error ao copiar para ${processedDataFile}!`)
      });
    }).on('close', function () {
      console.log(`Arquivo fechado: ${dataFile}`);
    });

  async function encontraTipoLogradouro(nometipoLogradouro, nome, row) {
    let tipo;
    if (!nometipoLogradouro)
      tipo = await TipoLogradouro.desconhecido();
    else {
      tipo = cacheTipoLogradouro[nometipoLogradouro];

      if (!tipo)
        tipo = await TipoLogradouro.findOneByNome(nometipoLogradouro)
        .then(async (tipo) => {
          if (!tipo) {
            tipo = await extractTipoLogradouroFromNome(nome);
            if (!tipo) {
              console.log("Falha ao encontrar o típo de logradouro! na linha %d, arquivo %s", countLineFile, dataFile);
              console.log(nometipoLogradouro);
              console.log(row);
              process.exit();
            }
          }

          cacheTipoLogradouro[nometipoLogradouro] = tipo;
          return tipo;
        })
        .catch((err) => {
          console.log("Erro na pesquisa do Tipo de Logradouro para  a linha: ");
          console.log(row);
          console.log(err);
          throw new Error(err);
        });
    }
    return tipo;
  }
}

async function encontrarLocalidade(nomeLocalidade, row) {
  let b;
  if (!b)
    b = await Bairro.findByNome(nomeLocalidade)
    .then(b => {
      return b;
    })
    .catch((err) => {
      console.log("Erro na pesquisa da Bairro para a linha: ");
      console.log(row);
      console.log(err);
      throw new Error(err);
    });
  if (!b)
    b = await SubDistrito.findByNome(nomeLocalidade)
    .then(b => {
      return b;
    })
    .catch((err) => {
      console.log("Erro na pesquisa do SubDistrito para  a linha: ");
      console.log(row);
      console.log(err);
      throw new Error(err);
    });
  if (!b)
    b = await Distrito.findByNome(nomeLocalidade)
    .then(b => {
      return b;
    })
    .catch((err) => {
      console.log("Erro na pesquisa do Distrito para  a linha: ");
      console.log(row);
      console.log(err);
      throw new Error(err);
    });
  return b;
}

async function encontraMunicipio(cep, row) {
  let m;
  let cepMunicipio;
  if (cep) cepMunicipio = cep.slice(0, 4);
  if (cepMunicipio) {
    m = cacheMunicipio[cepMunicipio];
    if (!m)
      m = await Municipio.findByCEP(cep)
      .then(m => {
        if (m)
          cacheMunicipio[cepMunicipio] = m;
        return m;
      })
      .catch((err) => {
        console.log("Erro na Pequisa do Muinicipio paraa linha: ");
        console.log(row);
        console.log(err);
        throw new Error(err);
      });
  }
  return m;
}

async function encontraUF(row) {
  let siglaEstado;
  try {
    isoSiglaEstado = row[1].split('.')[1]; // processa e obtem o iso
    siglaEstado = isoSiglaEstado.split('_')[1]; // processa e obtem a sigla simples
    siglaEstado = siglaEstado.toUpperCase();
  } catch (err) {
    console.log("Registro mal formatoado na linha %d, arquivo %s", countLineFile, dataFile);
    console.log(row);
    return;
  }
  let uf = cacheUFs[siglaEstado];
  if (!uf)
    uf = await UF.findBySigla(siglaEstado)
    .then(uf => {
      if (!uf) {
        console.log("UF é obrigátorio ser encontrado, verifique: %s", siglaEstado);
        process.exit(1); // uf é obrigatório existir se não a base é invalida.
      }
      cacheUFs[siglaEstado] = uf;
      return uf;
    }).catch((err) => {
      console.log("Erro  na pesquisa da UF da linha: ");
      console.log(row);
      console.log(err);
      throw new Error(err);
    });
  return uf;
}