var fs = require('fs');
var parse = require('csv-parse');


const Posto = require('../models/posto-atendimento');
const Uf = require('../models/logradouros/uf');
const Municipio = require('../models/logradouros/municipio');
const Microregiao = require('../models/logradouros/microregiao');
const Mesoregiao = require('../models/logradouros/mesoregiao');

module.exports = function (manager, next) {

  let errors = [];
  let count = 0;
  
  let dataFile = './dados/cities.csv';
  if (fs.existsSync(dataFile))
    fs.createReadStream(dataFile)
    .pipe(parse({
      delimiter: ',',
      // headers: [ibge_code,state_code,name,is_capital,lon,lat,no_accents,alternative_names,microregion,mesoregion]
    }))
    .on('headers', (header) => {
      console.log("Cabeçalho cities: " + header);
    })
    .on('data', (csvrow) => {
      if (csvrow[0] === 'ibge_code') {
        console.log("Cabeçalho cities: " + csvrow);;
        return
      }
      //      console.log("Muncipio: " + csvrow);

      //    0          1       2       3      4   5       6            7                8         9
      //ibge_code,state_code,name,is_capital,lon,lat,no_accents,alternative_names,microregion,mesoregion
      //1100015,RO,Alta Floresta D'Oeste,,-61.9998238963,-11.9355403048,Alta Floresta D'Oeste,,Cacoal,Leste Rondoniense
      //  console.log(csvrow)
      const idIBGEMicroregiao = csvrow[8];

      Microregiao.findByNome(idIBGEMicroregiao).then((microregiao) => {
        return Mesoregiao.findByNome(csvrow[9]).then((mesoregiao) => {
          return Municipio.findByIdIBGE(csvrow[0]).then((m) => {
            if (!m) {
              try {
                Municipio.create({
                  'idIBGE': csvrow[0],
                  'uf': csvrow[1],
                  'nome': csvrow[2],
                  'capital': Boolean(csvrow[3]),
                  gps: {
                    'longitude': csvrow[4],
                    'latitude': csvrow[5],
                  },
                  'apelido': csvrow[7],
                  'microregion': microregiao ? microregiao.id : null, // adotar algoritmo similar a localidade com distrito para atualizar a região do municipio
                  'mesoregion': mesoregiao ? mesoregiao.id : null,
                }).then(() => {
                  ++count
                });
              } catch (e) {
                console.log(csvrow);
                console.log(m);
                console.log(e);
                process.exit();
                errors.push(e);
                return e
              }
            }
          })
        })
      });
    })
    .on('end', function () {
      console.log('Leitura do Arquivo Cities finalizada;');
      if (errors.length) {
        throw new Error(errors[0]);
      }
      if (next) next(manager);
    })
    else if (next) next(manager);
}