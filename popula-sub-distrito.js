var fs = require('fs');
var parse = require('csv-parse');

const Municipio = require('../models/logradouros/municipio');
const {
	Localidade,
	Distrito,
	SubDistrito
} = require('../models/logradouros/localidade');

module.exports = function (manager, next) {
	let csvData = [];
	console.log("Iniciando Processamento de SubDistritos!");
	let dataFile = './dados/subdistricts.csv';
	if (fs.existsSync(dataFile))
		fs.createReadStream(dataFile)
		.pipe(
			parse({
				delimiter: ',',
				// headers: [ ibge_code,name,ibge_city_code,ibge_district_code ]
			})
		)
		.on('headers', header => {
			console.log('Cabeçalho sbudistricts: ' + header);
		})
		.on('data', csvrow => {
			if (csvrow[0] === 'ibge_code') {
				console.log('Cabeçalho subdistricts: ' + csvrow);
				return;
			}
			csvData.push(csvrow);
		})
		.on('end', async function () {
			console.log('Leitura do Arquivo subdistricts finalizada, linhas processadas: ' + csvData.length);
			let ibgeCityCode = [];
			let ibgeDistrictCode = [];
			let listSubdistritosNaoencontrados = [];
			let listFindCities = [];
			for (let row of csvData) {
				//    0       1         2                3
				//ibge_code,name,ibge_city_code,ibge_district_code
				//11002050506,Zona 01,1100205,110020505
				const ibgeCode = row[0];
				ibgeCityCode[ibgeCode] = row[2];
				ibgeDistrictCode[ibgeCode] = row[3];
				const nome = row[1];

				// console.log("Dados: " + csvrow);
				listFindCities.push(
					await SubDistrito.findByIdIBGE(ibgeCode)
					.then(subdistrito => {
						let distritoData;
						if (!subdistrito) {
							distritoData = {
								idIBGE: ibgeCode,
								nome: nome,
							};
							listSubdistritosNaoencontrados.push(distritoData);
						}
						return distritoData;
					})
				);
			}
			let race = Promise.all(listFindCities);
			let dsmUpdate = [];
			let listFindMunicipioDistrito = [];
			if (listSubdistritosNaoencontrados.length > 0)
				race.then(
					await SubDistrito.create(listSubdistritosNaoencontrados)
					.then(allDsM => {
						for (dsm of allDsM) {
							(function (dsm) {
								const idIBGE = dsm.idIBGE;
								const idIBGEMunicipio = ibgeCityCode[idIBGE];
								//  console.log(id2, ' - ', id)
								listFindMunicipioDistrito.push(Municipio.findByIdIBGE(idIBGEMunicipio)
									.then((m) => {
										dsm.municipio = m;
										Distrito.findByIdIBGE(ibgeDistrictCode[dsm.idIBGE])
											.then(d => {
												dsm.distrito = d;
												dsmUpdate.push(dsm);
											})
											.catch(err => {
												throw err;
											});
									}));
							})(dsm);
						}
						return allDsM;
					})
					.then((allDsM) => {
						console.log('SubDistritos inseridos:' + allDsM ? allDsM.length : 0);
					})
					.catch(err => {
						throw err;
					})
				)
				.then(Promise.all(listFindMunicipioDistrito))
				.then(() => {
					for (dsm of dsmUpdate) {
						dsm.save();
					}
					return dsmUpdate;
				}).then((allDsM) => {
					console.log('SubDistritos atualizados:' + allDsM ? allDsM.length : 0);
					if (next) next(manager);
				})
			else if (next) next(manager);
		});

	else if (next) next(manager);
};