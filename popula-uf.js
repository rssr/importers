var fs = require('fs');
var parse = require('csv-parse');


const Uf = require('../models/logradouros/uf');


module.exports = function (manager, next) {
  let errors = [];
  let dataFile = './dados/states.csv';
  if (fs.existsSync(dataFile))
    fs.createReadStream(dataFile)
    .pipe(parse({
      delimiter: ',',
      // headers: [ code,ibge_code,iso3166-2,osm_id,wikidata_id,name,no_accents,wikipedia_pt,website,timezone,flag_image]
    }))
    .on('headers', (header) => {
      console.log("Cabeçalho: " + header);
    })
    .on('data', async (csvrow) => {
      if (csvrow[1] === 'ibge_code') {
        console.log("Cabeçalho UF: " + csvrow);;
        return
      }
      //        console.log("Dados: " + csvrow);
      //  0      1        2        3                  4        5        6          7          8        9                 10
      //code,ibge_code,iso3166-2,osm_id         ,wikidata_id,name ,no_accents,wikipedia_pt,website,timezone          ,flag_image
      //AC  ,12       ,BR-AC    ,relation/326266,Q40780     ,Acre ,Acre      ,pt:Acre     ,http://,America/Rio_Branco,https://
      let uf = await Uf.findOne({
        'idIBGE': csvrow[1]
      }).select();

      if (!uf) {
        try {
          uf = new Uf({
            'sigla': csvrow[0],
            'nome': csvrow[5],
            'idIBGE': csvrow[1],
            'flagFile': csvrow[10]
          });
          await uf.save()
            .catch((err) => {
              errors.push(e);
            });
        } catch (e) {
          errors.push(e);
          return
        }
      }
      //       console.log("UF Cadastrada: " + uf);
    })
    .on('end', function () {
      if (errors.length) throw new Error(errors);

      console.log('Leitura do Arquivo UFs finalizada;');
      if (next) next(manager);
    });
    else if (next) next(manager);
};